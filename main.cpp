﻿#include <iostream>
#include "matrix.hpp"

int main()
{
	setlocale(LC_ALL, "Rus");
	Matrix A(3, 3);
	std::cin >> A;
	std::cout << A.transpos(A) << std::endl;
	std::cout << A.determinat(A) << std::endl;
	std::cout << A.inv(A) << std::endl;
	return 0;
}
