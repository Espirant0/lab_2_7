#pragma once
#include <iostream>

    class Matrix {
    public:
        Matrix(int n, int m);
        Matrix(const Matrix& mat);
        Matrix& operator=(const Matrix& mat);
        Matrix operator+(const Matrix& mat);
        Matrix operator+=(const Matrix& mat);
        Matrix operator*(const Matrix& mat);
        Matrix operator-(const Matrix& mat);
        Matrix operator-=(const Matrix& mat);
        int determinat(const Matrix& mat);
        Matrix inv(const Matrix& mat);
        Matrix transpos(const Matrix& mat);
        ~Matrix();

        friend std::istream& operator>>(std::istream& os, Matrix& mat);
        friend std::ostream& operator<<(std::ostream& os, const Matrix& mat);

    private:
        int m_n;
        int m_m;
        double** m_mat;
    };

